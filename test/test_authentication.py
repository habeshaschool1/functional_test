#!/usr/bin/env python3
"""this test will signup
login with correct user name and password ==> login session key should be replied
login with incorrect credentials ==> we should fail to get login session key
 """

from helper.api import STATUS_UNPROCESSABLE_ENTITY
from helper.authentication import *
from helper.database.mongo_db import delete_all_except, COLLECTION_USERS, DOCUMENT_KEY_USER_ID
from helper.user import POST_DATA
from helper.utils import generate_mandatory_fields, generate_string
from helper.utils import uniquify

mandatoryFieldsData = generate_mandatory_fields(POST_DATA)


def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_USERS, DOCUMENT_KEY_USER_ID, EXCLUDE_IDS)


def test_post_signup_signin_user():
    """
    this test will signup a user with valid field values
    and sign in to the user account
    system should return the signin success bearer token
    """

    # prepare a signup post data
    rand_number = generate_string(number=4)
    body = uniquify(mandatoryFieldsData, rand_number)

    # post to signup
    resp = post_signup(body, status=STATUS_CREATED)
    id = resp["result"]["user_id"]

    # assert id is a 6 digit int type
    assert isinstance(id, int) and len(str(id)) == 6
    # login to system
    correct_email = body["email"]
    correct_password = body["password"]
    resp = post_signin(correct_email, correct_password, expected_code=STATUS_CREATED)

    # assert response contains, login token
    assert resp["result"]["access_token"]


def test_signin_user_with_wrong_credentials():
    """ This test will signup a user
    and try to login with incorrect user name and password combination
     we should get login fail """

    # prepare a signup post data
    rand_number = generate_string(number=4)
    body = uniquify(mandatoryFieldsData, rand_number)

    # post to signup
    resp = post_signup(body, status=STATUS_CREATED)

    # login to system
    correct_email = body["email"]
    correct_password = body["password"]
    post_signin(correct_email, correct_password, expected_code=STATUS_CREATED)
    post_signin(correct_email, "", expected_code=STATUS_UNPROCESSABLE_ENTITY)
    post_signin(correct_email, "sdfsdfsdfsf", expected_code=STATUS_UNPROCESSABLE_ENTITY)
    post_signin("", correct_password, expected_code=STATUS_UNPROCESSABLE_ENTITY)
    post_signin("sdfsdfsdfds", correct_password, expected_code=STATUS_UNPROCESSABLE_ENTITY)
    post_signin("asdfasdfsaf", "asdfasdfsdafafa", expected_code=STATUS_UNPROCESSABLE_ENTITY)
    post_signin("", "", expected_code=STATUS_UNPROCESSABLE_ENTITY)