#!/usr/bin/env python3

import pytest

from helper.agenda import AGENDA_ID_OF_STATE_RATE, AGENDA_ID_OF_STATE_CLOSE, AGENDA_ID_OF_STATE_VOTE, \
    AGENDA_ID_OF_STATE_FORUM, AGENDA_ID_OF_STATE_REJECT, AGENDA_ID_OF_STATE_CANCEL, AGENDA_ID_OF_STATE_REVIEW, \
    AGENDA_ID_OF_STATE_EDIT
from helper.api import STATUS_CREATED, STATUS_OK, STATUS_NOT_AUTHORISED
from helper.database.mongo_db import delete_all_except, DOCUMENT_KEY_AGENDA_ID, COLLECTION_RATES
from helper.master import USER_ADMIN_ID
from helper.rate import get_rates, post_rates, POST_DATA, EXCLUDE_IDS
from helper.utils import generate_mandatory_fields, generae_missing_fields, generate_all_fields, uniquify

mandatoryFieldsData = generate_mandatory_fields(POST_DATA)
missingMandatoryFieldData = generae_missing_fields(mandatoryFieldsData)
allTestData = generate_all_fields(POST_DATA)
agenda_id = AGENDA_ID_OF_STATE_RATE


def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_RATES, DOCUMENT_KEY_AGENDA_ID, EXCLUDE_IDS)
    pass


@pytest.fixture(scope="class")
def test_mandatory_fields():
    """ post with only mandatory files ==> should get 201"""
    # prepare all (mandatory and optional) fields with +ve data
    # post
    resp = post_rates(agenda_id, like=True, status=STATUS_CREATED)
    id = resp["result"]["agenda_id"]
    user_id = resp["result"]["user_id"]

    # make suer agenda id is correct and  user id is admin
    assert int(id) == agenda_id
    assert str(user_id) == USER_ADMIN_ID


    # get user and validate phone exist
    get_rates(agenda_id=agenda_id, expected_code=STATUS_OK, expected_list="like:true")


@pytest.mark.parametrize("key, body, expectedRespCode", missingMandatoryFieldData)
def test_missing_mandatory_fields(test_mandatory_fields, key, body, expectedRespCode):
    """ post with with missing mandatory fields ==> should fail """
    # post the action
    body = uniquify(body)
    resp = post_rates(agenda_id, body=body, status=expectedRespCode)


@pytest.mark.parametrize("key, value, jsonBody, expectedRespCode", allTestData)
def test_all_fields(test_mandatory_fields, key, value, jsonBody, expectedRespCode):
    """ Creates with all fields ==> expect
    correct data ==>success (201) or
    incorrect data ==> fail 402
    """
    payload = uniquify(jsonBody)

    # post the action
    resp = post_rates(agenda_id, payload, status=expectedRespCode)


def test_rate_like_dislike():
    """
    this test is to validate if I am able to change my rate between True and False
    Steps:
        create agenda
        rate to True and validate
        change rate to False and validate
        and change rate to True again and validate
        """
    # post
    resp = post_rates(agenda_id=agenda_id, like=True, status=STATUS_CREATED)

    # validate like is true
    get_rates(agenda_id=agenda_id, expected_code=STATUS_OK, expected_list="like:true")

    # change like to false
    post_rates(agenda_id=agenda_id, like=False, status=STATUS_CREATED)

    # validate like is false
    get_rates(agenda_id=agenda_id, expected_code=STATUS_OK, expected_list="like:false")

    # change like to true
    post_rates(agenda_id=agenda_id, like=True, status=STATUS_CREATED)

    # validate like is true
    get_rates(agenda_id=agenda_id, expected_code=STATUS_OK, expected_list="like:true")


@pytest.mark.parametrize("agenda_id", [AGENDA_ID_OF_STATE_EDIT,
                                       AGENDA_ID_OF_STATE_REVIEW,
                                       AGENDA_ID_OF_STATE_CANCEL,
                                       AGENDA_ID_OF_STATE_REJECT,
                                       AGENDA_ID_OF_STATE_FORUM,
                                       AGENDA_ID_OF_STATE_VOTE,
                                       AGENDA_ID_OF_STATE_CLOSE]
                         )
def test_rate_agendas_which_are_not_in_state_rate_not_allowed(agenda_id):
    """
        this test will try to rate agendas which are not in state rate
    """

    # post
    post_rates(agenda_id=agenda_id, like=True, status=STATUS_NOT_AUTHORISED)
