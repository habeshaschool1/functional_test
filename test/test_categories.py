#!/usr/bin/env python3
""" This test_function will do all activities related to category service
    Get (all, one by one)
    Create, Update, Delete
 """

import pytest

from helper.api import STATUS_CREATED, STATUS_OK
from helper.category import POST_DATA, EXCLUDE_CATEGORY_IDS, post_categories, get_categories
from helper.database.mongo_db import delete_all_except, COLLECTION_CATEGORIES, DOCUMENT_KEY_USER_ID, \
    DOCUMENT_KEY_CATEGORY_ID
from helper.utils import generate_mandatory_fields, generae_missing_fields, generate_all_fields, uniquify, \
    generate_string

""" will first try to create with only mandatory fields and correct data if that passed then
    will continue to test_function by different data and missed attributes for +ve/-ve test_function
"""
mandatoryFieldsData = generate_mandatory_fields(POST_DATA)
missingMandatoryFieldData = generae_missing_fields(mandatoryFieldsData)
allTestData = generate_all_fields(POST_DATA)

def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_CATEGORIES, DOCUMENT_KEY_CATEGORY_ID, EXCLUDE_CATEGORY_IDS)
    pass


@pytest.fixture(scope="class")
def test_mandatory_fields():
    """ post with only mandatory files ==> should get 201"""
    # prepare all (mandatory and optional) fields with +ve data
    salt_str = generate_string(upper=5)
    payload = uniquify(mandatoryFieldsData, salt_str)
    # post
    resp = post_categories(payload, status=STATUS_CREATED)
    category_id = resp["result"]["category_id"]

    # get user and validate phone exist
    get_categories(category_id, expected_code=STATUS_OK)


@pytest.mark.parametrize("key, body, expectedRespCode", missingMandatoryFieldData)
def test_missing_mandatory_fields(test_mandatory_fields, key, body, expectedRespCode):
    """ post with with missing mandatory fields ==> should fail """
    # post the action
    salt_str = generate_string(upper=5)
    body = uniquify(body, salt_str)
    resp = post_categories(body, status=expectedRespCode)

@pytest.mark.parametrize("key, value, jsonBody, expectedRespCode", allTestData)
def test_all_fields(test_mandatory_fields, key, value, jsonBody, expectedRespCode):
    """ Creates with all fields ==> expect
    correct data ==>success (201) or
    incorrect data ==> fail 402
    """
    salt_str = generate_string(upper=5)
    payload = uniquify(jsonBody, salt_str)

    # post the action
    resp = post_categories(payload, status=expectedRespCode)

