import pytest
""" Kalai 11/03/2020"""
from helper.agenda import AGENDA_ID_OF_STATE_EDIT, AGENDA_STATE_EDIT, AGENDA_ID_OF_STATE_FORUM, \
    AGENDA_ID_OF_STATE_REVIEW, AGENDA_ID_OF_STATE_CANCEL, AGENDA_ID_OF_STATE_RATE, AGENDA_ID_OF_STATE_REJECT, \
    AGENDA_ID_OF_STATE_VOTE, AGENDA_ID_OF_STATE_CLOSE, AGENDA_STATE_CLOSE, AGENDA_STATE_VOTE, AGENDA_STATE_FORUM, \
    AGENDA_STATE_REJECT, AGENDA_STATE_RATE, AGENDA_STATE_CANCEL, AGENDA_STATE_REVIEW, post_agendas, \
    post_an_agenda_and_set_state, reset_state_of_test_agendas, EXCLUDE_IDS
from helper.api import STATUS_CREATED
from helper.dashboard import get_dashboard
from helper.database.mongo_db import delete_all_except, COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID
from helper.forum import post_forum
from helper.rate import put_rates, post_rates
from helper.vote import post_vote, vote_yes, vote_no, vote_abstain


def setup_function():
    """ reset all agendas from 1 to 9 to original state"""
    reset_state_of_test_agendas()


def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, EXCLUDE_IDS)


def test_dashboard_agendas_are_categorised_on_a_right_way():
    """this test is to validate dashboard is able to give all agendas
    agendas on Edit mode
    agendas on Review mode
    agendas on CANCEL mode
    agendas on RATE mode
    agendas on REJECT mode
    agendas on FORUM mode
    agendas on VOTE mode
    agendas on CLOSE mode
        """
    resp = get_dashboard()
    owned = resp["owned"]
    assert len(owned["agendas"]) >= 0;
    assert AGENDA_ID_OF_STATE_EDIT in owned[AGENDA_STATE_EDIT]
    assert AGENDA_ID_OF_STATE_REVIEW in owned[AGENDA_STATE_REVIEW]
    assert AGENDA_ID_OF_STATE_CANCEL in owned[AGENDA_STATE_CANCEL]
    assert AGENDA_ID_OF_STATE_RATE in owned[AGENDA_STATE_RATE]
    assert AGENDA_ID_OF_STATE_REJECT in owned[AGENDA_STATE_REJECT]
    assert AGENDA_ID_OF_STATE_FORUM in owned[AGENDA_STATE_FORUM]
    assert AGENDA_ID_OF_STATE_VOTE in owned[AGENDA_STATE_VOTE]
    assert AGENDA_ID_OF_STATE_CLOSE in owned[AGENDA_STATE_CLOSE]


@pytest.mark.parametrize("state", [AGENDA_STATE_EDIT,
                                   AGENDA_STATE_REVIEW,
                                   AGENDA_STATE_CANCEL,
                                   AGENDA_STATE_RATE,
                                   AGENDA_STATE_REJECT,
                                   AGENDA_STATE_FORUM,
                                   AGENDA_STATE_VOTE,
                                   AGENDA_STATE_CLOSE])
def test_dashboard_edit_state(state):
    """
    this test is to validate the count of edit state agenda is correct or not
    Steps:
        get current dashboard report
        get agendas on the given state
        create a new agenda and set it to the given state ==> state1
        get dashboard report again
        get all all agendas on the given state states ==> state2
        validate count of state2 == state1 +1
        validate the new agenda id is in state2 list

        """
    # get dashboard
    resp = get_dashboard()
    agendas_on_given_state = resp["owned"][state]

    # post an agenda to be on the given state
    agenda_id = post_an_agenda_and_set_state(state=state, status=STATUS_CREATED)

    # get dashboard again
    resp = get_dashboard()
    agendas_on_given_state2 = resp["owned"][state]
    # assert state2 contains the new agenda_id
    assert agenda_id in agendas_on_given_state2
    # assert state count increased by one
    assert len(agendas_on_given_state2) == len(agendas_on_given_state) + 1


def test_dashboard_like_dislike_counts_are_correct():
    """ this test is to validate the like/dislike count of dashboard is correct
    steps:
        get current like dislike count
        post a new agenda and set to Rate state
        like the agenda
        get like dislike counts again ==> like count should increase by one
        dislike the agenda
        get like/dislike counts ==> dislike count should increase by one
        """

    # get dashboard
    resp = get_dashboard()
    like_agendas = resp["like"]
    dislike_agendas = resp["dislike"]

    # post an agenda to be on the given state
    agenda_id = post_an_agenda_and_set_state(state=AGENDA_STATE_RATE, status=STATUS_CREATED)

    # post to like the above agenda
    post_rates(agenda_id, like=True, status=STATUS_CREATED)

    # get dashboard
    resp = get_dashboard()
    like_agendas2 = resp["like"]
    dislike_agendas2 = resp["dislike"]

    # validate like count increased by one
    assert agenda_id in like_agendas2
    assert len(like_agendas2) == len(like_agendas) + 1
    assert len(dislike_agendas2) == len(dislike_agendas)

    # post to dislike the above agenda
    post_rates(agenda_id, like=False, status=STATUS_CREATED)

    # get dashboard
    resp = get_dashboard()
    like_agendas2 = resp["like"]
    dislike_agendas2 = resp["dislike"]

    # validate like count increased by one
    assert agenda_id in dislike_agendas2
    assert len(like_agendas2) == len(like_agendas)
    assert len(dislike_agendas2) == len(dislike_agendas) + 1


def test_dashboard_total_comments():
    """ this test is to validate the total comments count is correct on dashboard
    first get the current total comment from dashboard ==> count1
    make a comment to an agenda
    then get the total comment again ==> count2
    validate that count2 = count1 + 1 """

    # get current total comment
    resp = get_dashboard()
    total_comment1 = resp["total_comments"]

    # make a comment to an agenda on forum state
    post_forum(AGENDA_ID_OF_STATE_FORUM, "this is my comment", status=STATUS_CREATED)

    # count again ==> then the count should increase by one
    resp = get_dashboard()
    total_comment2 = resp["total_comments"]

    assert total_comment2 == total_comment1 + 1


@pytest.mark.parametrize("vote", [vote_yes, vote_no, vote_abstain])
def test_dashboard_vote_yes_no_abstain_counts_are_correct(vote):
    """ this test is to validate vote yes/no/abstain count of dashboard is correct
    steps:
        get current vote yes/no/abstain agendas
        post a new agenda and set to Vote state
        vote the agenda to one of yes, no or abstain
        get report again ==> then vote count should be increased according to your vote type
        """

    # get dashboard
    resp = get_dashboard()
    yes_agendas = resp["yes"]
    no_agendas = resp["no"]
    abstain_agendas = resp["abstain"]

    # post an agenda to be on the given state
    agenda_id = post_an_agenda_and_set_state(state=AGENDA_STATE_VOTE, status=STATUS_CREATED)

    # post to like the above agenda
    post_vote(agenda_id, vote, expected_code=STATUS_CREATED)

    # get dashboard
    resp = get_dashboard()
    yes_agendas2 = resp["yes"]
    no_agendas2 = resp["no"]
    abstain_agendas2 = resp["abstain"]

    # validate like count increased by one
    if vote == vote_yes:
        assert agenda_id in yes_agendas2
        assert len(yes_agendas2) == len(yes_agendas) + 1
        assert len(no_agendas2) == len(no_agendas)
        assert len(abstain_agendas2) == len(abstain_agendas)
    elif vote == vote_no:
        assert agenda_id in no_agendas2
        assert len(yes_agendas2) == len(yes_agendas)
        assert len(no_agendas2) == len(no_agendas) + 1
        assert len(abstain_agendas2) == len(abstain_agendas)
    elif vote == vote_abstain:
        assert agenda_id in abstain_agendas2
        assert len(yes_agendas2) == len(yes_agendas)
        assert len(no_agendas2) == len(no_agendas)
        assert len(abstain_agendas2) == len(abstain_agendas) + 1


