#!/usr/bin/env python3

import pytest

from helper.agenda import AGENDA_ID_OF_STATE_FORUM, AGENDA_ID_OF_STATE_EDIT, AGENDA_ID_OF_STATE_REVIEW, \
    AGENDA_ID_OF_STATE_CANCEL, AGENDA_ID_OF_STATE_REJECT, AGENDA_ID_OF_STATE_VOTE, AGENDA_ID_OF_STATE_CLOSE, \
    AGENDA_ID_OF_STATE_RATE
from helper.api import STATUS_CREATED, STATUS_OK, STATUS_NOT_AUTHORISED, STATUS_REQUEST_ERROR
from helper.forum import get_forum, post_forum
from helper.master import USER_ADMIN_ID
from helper.utils import generate_string

agenda_id = AGENDA_ID_OF_STATE_FORUM


@pytest.mark.parametrize("agenda_id, comment", [(AGENDA_ID_OF_STATE_FORUM, generate_string(lower=10))])
def test_comment_on_agenda_which_is_in_forums_state(agenda_id, comment):
    """ this test is just to post a comment on a forum which is on forums state
    post should be accepted
    steps:
        login should be required
        get agenda which is on forums state
        post a comment ==> then post hsould be successfull
        get the the comment ==> the nthe posted comment should be seen in the response
        """
    resp = post_forum(agenda_id, comment=comment, status=STATUS_CREATED)
    id = resp["result"]["agenda_id"]
    user_id = resp["result"]["user_id"]

    # make suer agenda id is correct and  user id is admin
    assert int(id) == agenda_id
    assert str(user_id) == USER_ADMIN_ID

    # get get forum
    get_forum(agenda_id=agenda_id, user_id=USER_ADMIN_ID, expected_code=STATUS_OK, expected_list=comment)


@pytest.mark.parametrize("comment", [None, ""])
def test_post_forum_with_wrong_data(comment):
    """ this test is try to post comment with wrong value ==> should throw error"""
    post_forum(agenda_id, comment=comment, status=STATUS_REQUEST_ERROR)


@pytest.mark.skip(" Not shoure how to make our api test to post comment without login")
def test_comment_without_login():
    #TODO try to post comment without loging ==> should fail
    pass


@pytest.mark.parametrize("agenda_id", [AGENDA_ID_OF_STATE_EDIT,
                                       AGENDA_ID_OF_STATE_REVIEW,
                                       AGENDA_ID_OF_STATE_CANCEL,
                                       AGENDA_ID_OF_STATE_REJECT,
                                       AGENDA_ID_OF_STATE_RATE,
                                       AGENDA_ID_OF_STATE_VOTE,
                                       AGENDA_ID_OF_STATE_CLOSE]
                         )
def test_comment_to_agenda_which_is_not_in_forums_state_not_allowed(agenda_id):
    post_forum(agenda_id, comment="sample text", status=STATUS_NOT_AUTHORISED)
