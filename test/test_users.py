#!/usr/bin/env python3
""" This test_function will do all activities related to test1 service
    Get (all, one by one)
    Create, Update, Delete
 """

import pytest

from helper.api import STATUS_UNPROCESSABLE_ENTITY
from helper.database.mongo_db import delete_all_except, COLLECTION_USERS, DOCUMENT_KEY_USER_ID
from helper.master import USER_ADMIN_EMAIL
from helper.user import get_user, POST_DATA, get_current_user
from helper.utils import generate_all_fields, generae_missing_fields, generate_mandatory_fields, generate_string
from helper.authentication import *
from helper.utils import uniquify

""" will first try to create with only mandatory fields and correct data if that passed then
    will continue to test_function by different data and missed attributes for +ve/-ve test_function
"""
mandatoryFieldsData = generate_mandatory_fields(POST_DATA)
missingMandatoryFieldData = generae_missing_fields(mandatoryFieldsData)
allTestData = generate_all_fields(POST_DATA)


def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_USERS, DOCUMENT_KEY_USER_ID, EXCLUDE_IDS)
    pass


@pytest.fixture(scope="class")
def test_mandatory_fields():
    """ post with only mandatory files ==> should get 201"""
    # prepare all (mandatory and optional) fields with +ve data
    rand_number = generate_string(number=4)
    payload = uniquify(mandatoryFieldsData, rand_number)

    # post
    resp = post_signup(payload, status=STATUS_CREATED)
    user_id = resp["result"]["user_id"]

    # get user and validate phone exist
    expected_str = f"phone: {payload['phone']}"
    get_user(user_id, expected_code=STATUS_OK, expected_list=expected_str)


@pytest.mark.parametrize("key, body, expectedRespCode", missingMandatoryFieldData)
def test_missing_mandatory_fields(test_mandatory_fields, key, body, expectedRespCode):
    """ post with with missing mandatory fields ==> should fail """
    # post the action
    rand_number = generate_string(number=4)
    body = uniquify(body, rand_number)

    resp = post_signup(body, status=expectedRespCode)


@pytest.mark.parametrize("key, value, jsonBody, expectedRespCode", allTestData)
def test_all_fields(test_mandatory_fields, key, value, jsonBody, expectedRespCode):
    """ Creates with all fields ==> expect
    correct data ==>success (201) or
    incorrect data ==> fail 402
    """
    rand_number = generate_string(number=4)
    payload = uniquify(jsonBody, rand_number)

    # post the action
    resp = post_signup(payload, status=expectedRespCode)

def test_get_current_user():
    """This is to get current user information without password"""
    resp = get_current_user(expected_code=STATUS_OK)
    assert resp["email"] == USER_ADMIN_EMAIL
    assert resp.get("password", "") == ""




