#!/usr/bin/env python3
""" This test_function will do all activities related to test1 service
    Get (all, one by one)
    Create, Update, Delete
 """

import pytest

from helper.agenda import POST_DATA, EXCLUDE_IDS, post_agendas, get_agendas, reset_state_of_test_agendas
from helper.api import STATUS_UNPROCESSABLE_ENTITY, STATUS_CREATED, STATUS_OK
from helper.database.mongo_db import delete_all_except, COLLECTION_AGENDAS, \
    DOCUMENT_KEY_AGENDA_ID
from helper.utils import generate_all_fields, generae_missing_fields, generate_mandatory_fields, generate_string, uniquify

""" will first try to create with only mandatory fields and correct data if that passed then
    will continue to test_function by different data and missed attributes for +ve/-ve test_function
"""
mandatoryFieldsData = generate_mandatory_fields(POST_DATA)
missingMandatoryFieldData = generae_missing_fields(mandatoryFieldsData)
allTestData = generate_all_fields(POST_DATA)

def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, EXCLUDE_IDS)

def setup_function():
    """ reset all agendas from 1 to 9 to original state"""
    reset_state_of_test_agendas()

@pytest.fixture(scope="class")
def test_mandatory_fields():
    """ post with only mandatory files ==> should get 201"""
    # prepare all (mandatory and optional) fields with +ve data
    post_fix_txt = generate_string(upper=4)
    payload = uniquify(mandatoryFieldsData, post_fix_txt)

    # post
    resp = post_agendas(payload, status=STATUS_CREATED)
    agenda_id = resp["result"]["agenda_id"]

    # get user and validate phone exist
    get_agendas(agenda_id, expected_code=STATUS_OK)


@pytest.mark.parametrize("key, body, expectedRespCode", missingMandatoryFieldData)
def test_missing_mandatory_fields(test_mandatory_fields, key, body, expectedRespCode):
    """ post with with missing mandatory fields ==> should fail """
    # post the action
    post_fix_txt = generate_string(upper=4)
    body = uniquify(body, post_fix_txt)

    resp = post_agendas(body, status=expectedRespCode)


@pytest.mark.parametrize("key, value, jsonBody, expectedRespCode", allTestData)
def test_all_fields(test_mandatory_fields, key, value, jsonBody, expectedRespCode):
    """ Creates with all fields ==> expect
    correct data ==>success (201) or
    incorrect data ==> fail 402
    """
    post_fix_txt = generate_string(upper=4)
    payload = uniquify(jsonBody, post_fix_txt)

    # post the action
    resp = post_agendas(payload, status=expectedRespCode)


