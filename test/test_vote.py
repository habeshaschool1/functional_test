#!/usr/bin/env python3
""" syamala test_vote"""
import pytest

from helper.agenda import AGENDA_STATE_VOTE, EXCLUDE_IDS, post_an_agenda_and_set_state
from helper.api import STATUS_CREATED, STATUS_OK, STATUS_NO_URI, STATUS_UNPROCESSABLE_ENTITY
from helper.database.mongo_db import delete_all_except, COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, \
    DOCUMENT_KEY_USER_ID, COLLECTION_VOTES, COLLECTION_LOCKS
from helper.master import USER_ADMIN_ID
from helper.vote import post_vote, get_vote, vote_yes, vote_no, vote_abstain


def teardown_module():
    # delete all except the given ids
    delete_all_except(COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, EXCLUDE_IDS)
    delete_all_except(COLLECTION_VOTES, DOCUMENT_KEY_USER_ID, [])
    delete_all_except(COLLECTION_LOCKS, DOCUMENT_KEY_USER_ID, [])


@pytest.mark.parametrize("my_vote", [vote_yes, vote_no, vote_abstain,"", None, 123, "any text"])
def test_post_vote_with_valid_data(my_vote):
    """ this test is to validate votes with correct values (yes, no, abstain)
    Steps:
        create a new agenda and set to vote ==> so that I can vote
        send a post to vote for the first time(yes, or not, or abstain) ==> post should be success
        get vote and validate you have the right vote
        """
    # create a new agenda and set status to vote
    agenda_id = post_an_agenda_and_set_state(state=AGENDA_STATE_VOTE, status=STATUS_CREATED)

    if my_vote in [vote_yes, vote_no, vote_abstain]:
        # vote for the first time ==> should success
        post_vote(agenda_id, my_vote, expected_code=STATUS_CREATED)
        # validate vote is done correctly
        expected_str = "vote:{}".format(my_vote)
        get_vote(agenda_id, USER_ADMIN_ID, expected_code=STATUS_OK, expected_list=expected_str)
    else:
        post_vote(agenda_id, my_vote, expected_code=STATUS_UNPROCESSABLE_ENTITY)


@pytest.mark.parametrize("vote", [vote_yes, vote_no, vote_abstain])
def test_vote_multiple_times_not_allowed(vote):
    """ this test case is to validate voting for seconding time is not allowed
    Steps:
        create a new agenda and set it Vote state
        vote any one of the list (yes, no, abstain) ==> validate vote is success
        try to change vote to other than the above ==> vote should note be allowed
        """

    # create a new agenda and set status to vote
    agenda_id = post_an_agenda_and_set_state(state=AGENDA_STATE_VOTE, status=STATUS_CREATED)

    # vote to yes for first time ==> should be success
    post_vote(agenda_id, vote, expected_code=STATUS_CREATED)
    # exclude your vote from the list
    vote_types = [vote_yes, vote_no, vote_abstain]
    vote_types.remove(vote)

    # vot to no and abstain ==> should fail
    for vote in vote_types:
        post_vote(agenda_id, vote, expected_code=STATUS_NO_URI)

