# the complete code to post a new user through authentication/signup endpoint
import requests
from urllib3.exceptions import InsecureRequestWarning

# find a library to create an api request session
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
session = requests.Session()

"""to signup you don't need Bearor token, 
if you want to do for end points use the following 
bearer = f'Bearer {user_bearer_token.strip()}'
        bearer_token = {'Authorization':bearer}
        session1.headers.update(bearer_token)
"""
session.headers.update({'Accept': 'application/json'})


def test_signup():
    uri = " http://127.0.0.1:5000/authentication/signup/"
    body = {"email": "abc@gmail.com", "password": "abc", "phone": "3017929865"}
    resp = session.post(url=uri, json=body)
    response_code =resp.status_code
    print(f"************Response status code ********{str(response_code)}********")
    print(resp.json())
    assert response_code == 201





