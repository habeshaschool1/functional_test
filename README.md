# api_testing_in_python_(PYTESt)
This test frame work has the following packages:

## A config file: 
Defines basic data we sue to config our api test:
* IP, PORT
* common user name, email, password 
* database credentials 
* ...
## helper package
where you will find all helper methods
* utils file : top file to be inherited by other files 
    * contains all generic teat functions like:
    * test data generator ( only mandatory, missed, and all combined data for Data Driven Testing)
    * date time format generators
    * random string generators
    * method to find sub-json in a complex json
    * find a key, value paris in a complex json
    * assert if string exists in a json 
    * ....
* master file: contains all features to pre-setup the application under test 
    * if user has to be created
    * if network, server, access credentials has to be set
    * if application requires a startup data, in the database 
    * this master file should accomplish it
* api helper file contains 
    * start a API session by (user name, and password or api_bearer session key)
    * list of all response codes to be used in our test
    * list of all end_points 
    * list of generic methods to POST, GET, PUT, PATCH, DELETE
    * this file can import functions from master and utils file
* all helper files for each test feature 
    * there should be a POST, PUT, GET, DELETE, PATCH, helpers to this feature
    * all possible (-v/+tv) test data should be stored here 
## schema package 
Contains schemas of important features for validations
     
## test package
Contains all the test files 
* each test features, like signup, signin user should have its own file
* each test should inherit from helper(api, util, and it's own helper file, or on other helper files) 
* test cases should contain the following:
    * test schema of GET data
    * test mandatory fields only ==> success response
    * test missing mandatory fields ==> 400 error response
    * test each field using various data types ==> result depends on field requirement
    * test data should be populated on the helper file of this test file
    * each test file should implement setup/teardown features if needed

## TODO list
* framework will include the following helper files
* helpers for relational, and non relational databases
* helpers for aws (DynamoDB, S3, Lambda, SQS, SNS features) 

