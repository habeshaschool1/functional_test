from helper.api import END_POINT_POST_SIGNUP, post, put, END_POINT_POST_SIGNIN, STATUS_OK, \
    STATUS_CREATED, get, delete, STATUS_DELETED, END_POINT_POST_CATEGORIES, END_POINT_GET_CATEGORIES, \
    END_POINT_DELETE_CATEGORIES, END_POINT_PUT_CATEGORIES

EXCLUDE_CATEGORY_IDS = [1, 2, 3]


def post_categories(body, status=None, expected_list=None):
    resp = post(END_POINT_POST_CATEGORIES, None, body, status, expected_list)
    return resp


def put_categories(id, title=None, desc=None, status=None, expected_list=None):
    body = {}
    if title is not None:
        body["title"] = title

    if desc is not None:
        body["desc"] = desc

    resp = put(END_POINT_PUT_CATEGORIES.format(id), None, body, status, expected_list)
    return resp


def get_categories(id, param=None, expected_code=None, expected_list=[]):
    resp = get(END_POINT_GET_CATEGORIES.format(id), params=param, expected_code=expected_code,
               expected_data_list=expected_list)
    return resp


def delete_categories(id, expected_code=STATUS_DELETED, expectedStr=None):
    resp = delete(END_POINT_DELETE_CATEGORIES.format(id), None, None, expected_code, expectedStr)
    return resp


def delete_all_categories(except_ids=[]):
    print("*" * 10 + "Clear all categories except the following  IDs")
    print(except_ids)
    resps = get_categories("", None, STATUS_OK, None)
    for resp in resps["result"]:
        id = resp["category_id"]
        if not id in except_ids:
            delete_categories(id, STATUS_DELETED, None)


POST_DATA = {
    "title": (["Sport{}", "Sport1{}"], ["#$#$", "", " ", "Sport$"]),
    "desc": (["this is my desc and should be more than 25 chars",
              "canb this also should be mroe thant 25  chars $$^$%#$ text to{}"
              ],
             ["can't be less than 25"]
             )
}
