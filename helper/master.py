#!/usr/bin/env python3
"""
    Contains common api_test constants, endpoints and
    methods toGet, Put, Post, Del Methods
    Assertion methods: to validate response code, value (string / json ) and schema
"""
import copy
import json

# access
import requests
from urllib3.exceptions import InsecureRequestWarning

from helper.utils import print_details, assert_data, load_config_file, basicAssert

# required endpoints to make a pre-setup, before test
END_POINT_POST_SIGNUP = "/authentication/signup/"
END_POINT_POST_SIGNIN = "/authentication/signin/"

# lod config data
config = load_config_file()
#IP  and PORT
IP = config["Host"]["ip"]
PORT = config["Host"]["port"]

# USER DETAIL
USER_SYSTEM_EMAIL = config["User"]["email_system"]
USER_SYSTEM_ID = config["User"]["id_system"]
USER_SYSTEM_PHONE = config["User"]["phone_system"]

USER_ADMIN_EMAIL = config["User"]["email_admin"]
USER_ADMIN_ID = config["User"]["id_admin"]
USER_ADMIN_PHONE = config["User"]["phone_admin"]

USER_NORMAL_EMAIL = config["User"]["email_user"]
USER_NORMAL_ID = config["User"]["id_user"]
USER_NORMAL_PHONE = config["User"]["phone_user"]

USER_COMMON_PASSWORD = config["User"]["password"]
USER_ACCESS_ADMIN = config["User"]["access"]

# MONGO DB
MONGO_IP = config["Mongo"]["mongo_ip"]
MONGO_PORT = config["Mongo"]["mongo_port"]
MONGO_DB = config["Mongo"]["mongo_db"]

BASE_URI = f"http://{IP}:{PORT}"

# api session
def create_session(email=None, password=None, user_bearer_token=None):

    """ Method to create api_test connection session, will be used to GET/POST/PUT/DEL
    :param email:
    :param password:
    """
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    session1 = requests.Session()
    if email and password: session1.auth = (email, password)
    if user_bearer_token is not None:
        bearer = f'Bearer {user_bearer_token.strip()}'
        bearer_token = {'Authorization':bearer}
        session1.headers.update(bearer_token)
    session1.headers.update({'Accept': 'application/json'})

    session1.verify = False
    return session1


def sign_in(email, password):
    try:
        body = {"email": email, "password": password}
        uri = BASE_URI + END_POINT_POST_SIGNIN
        print_details("POST", uri, None, body)
        session = create_session()
        resp = session.post(url=uri, json=body)
        resp = resp.json()
        access_token = resp["result"]["access_token"]
        return access_token
    except:
        return None

# sign in as admin user
USER_SYSTEM_BEARER_TOKEN = sign_in(email=USER_SYSTEM_EMAIL, password=USER_COMMON_PASSWORD)
USER_ADMIN_BEARER_TOKEN = sign_in(email=USER_ADMIN_EMAIL, password=USER_COMMON_PASSWORD)
USER_NORMAL_BEARER_TOKEN = sign_in(email=USER_NORMAL_EMAIL, password=USER_COMMON_PASSWORD)

USER_SYSTEM_SESSION = create_session(user_bearer_token=USER_SYSTEM_BEARER_TOKEN)
USER_ADMIN_SESSION = create_session(user_bearer_token=USER_ADMIN_BEARER_TOKEN)
USER_NORMAL_SESSION = create_session(user_bearer_token=USER_NORMAL_BEARER_TOKEN)


