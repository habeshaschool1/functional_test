from helper.api import END_POINT_POST_VOTES,  END_POINT_GET_VOTES, post, get, END_POINT_POST_AGENDAS, \
    END_POINT_GET_AGENDAS
from helper.forum import post, get
from helper.database.mongo_db import delete_all_except, COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, update_one
vote_yes = "yes"
vote_no = "no"
vote_abstain = "abstain"

def post_vote(agenda_id, vote, expected_code=None, expected_list=None):
    body = {"vote": vote}

    resp = post(END_POINT_POST_VOTES.format(agenda_id), None, body, expected_code, expected_list)
    return resp



def get_vote(agenda_id="", user_id=None, expected_code=None, expected_list=[]):
    params = None
    if user_id is not None:
        params = {"user_id": user_id}
    resp = get(END_POINT_GET_VOTES.format(agenda_id), params=params, expected_code=expected_code, expected_data_list=expected_list)
    return resp

