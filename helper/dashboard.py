""" Kalai 11/03/2020"""

from helper.api import END_POINT_GET_DASHBOARD, get


def get_dashboard(expected_code=None, expected_list=[]):
    resp = get(END_POINT_GET_DASHBOARD, expected_code=expected_code, expected_data_list=expected_list)
    return resp
