from helper.api import put, STATUS_OK, \
    STATUS_CREATED, get, delete, STATUS_DELETED, END_POINT_GET_USERS, END_POINT_PUT_USERS, \
    END_POINT_DELETE_USERS, END_POINT_GET_CURRENT_USER


def put_user(id, payload, expected_code, expected_list):
    resp = put(END_POINT_PUT_USERS.format(id), None, payload, expected_code, expected_list)
    return resp

def get_user(id, param=None, expected_code=None, expected_list=None):
    resp = get(END_POINT_GET_USERS.format(id), params=param, expected_code=expected_code, expected_data_list=expected_list)
    return resp

def get_current_user(expected_code=None, expected_list=None):
    resp = get(END_POINT_GET_CURRENT_USER, expected_code=expected_code, expected_data_list=expected_list)
    return resp

def delete_user(id, expected_code=STATUS_DELETED, expectedStr=None):
    resp = delete(END_POINT_DELETE_USERS.format(id), None, None, expected_code, expectedStr)
    return resp

def delete_all_users(except_ids=[]):
    print("*" * 10 + "Clear all User Profiles except the following  IDs")
    print(except_ids)
    resps = get_user("", None, STATUS_OK, None)
    for resp in resps:
        id = resp["id"]
        if not id in except_ids:
            delete_user(id, STATUS_DELETED, None)


POST_DATA = {
    "email": (
        ["a@gmail.com{}", "a@habeshaschool.com{}", "123@gmail.com{}", "a_b.c@gmail.com{}"],
        ["not email", "a@@gmail.com", "b@gmailcom"]
    ),
    "password": (["abdE2$", "abcdE3$",  "A", "abcdefaghijkljlkj","ASDFSDFSDFSDFSDFSD",
                                         "12344554344544", "$%$%$%$%$%^%$%$",
                                         "abcdeF$", "abcd1$", "SDFDFDFGD1$", "a1$F5"], [""]),
    "phone": (["301792{}"], ["", "asdfsd", "301-792-9864", "301-792-98656"]),
    "name": (["test_name{}", 123455, "$%$$%", "this is my name I am make"], ["", "        ", "this is my name I am makeing more than 15 characters"])

}