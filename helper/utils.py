#!/usr/bin/env python3
"""define generic constants, functions can be used by helper and test_function methods """
import configparser
import copy
import datetime
import json
import random
import socket
import string
import subprocess
import sys
from configparser import ConfigParser
from time import sleep

from jsonschema import validate
import re


DATE_UTC_FORMAT = "%Y-%m-%d %H:%M:%S %p UTC"
DATE_GM_FORMAT = "%Y:%m:%d:%H:%M:%S:%f"
DATE_TXT_FORMAT = "%Y%m%d%H%M%S%f"

def load_config_file():
    """ this method will read data from properties file"""
    config = ConfigParser()
    config.read('../config_file.properties')
    return config._sections

def get_date_days_from_now(daysFromNow, format=DATE_GM_FORMAT):
    """ method to return current dat with the given format"""
    now = None
    if format == DATE_UTC_FORMAT:
        now = datetime.datetime.utcnow()
    else:
        now = datetime.datetime.now()

    d = datetime.timedelta(days=abs(daysFromNow))
    exactDay = None
    if daysFromNow > 0:
        exactDay = now + d
    elif daysFromNow == 0:
        exactDay = now
    if daysFromNow < 0:
        exactDay = now - d
    exactDay = exactDay.strftime(format)
    print("*" * 10 + " Days generated now " + str(daysFromNow) + " == " + exactDay)

    return exactDay



def get_date_minutes_from_now(minutes, format=DATE_GM_FORMAT):
    """ method to return current dat with the given format"""
    now = None
    if format == DATE_UTC_FORMAT:
        now = datetime.datetime.utcnow()
    else:
        now = datetime.datetime.now()

    d = datetime.timedelta(minutes=abs(minutes))
    exactDay = None
    if minutes > 0:
        exactDay = now + d
    elif minutes == 0:
        exactDay = now
    if minutes < 0:
        exactDay = now - d
    exactDay = exactDay.strftime(format)
    print("*" * 10 + " Minutes generated now " + str(minutes) + " == " + exactDay)

    return exactDay



def get_date_seconds_from_now(seconds, format=DATE_GM_FORMAT):
    """ method to return current dat with the given format"""
    now = None
    if format == DATE_UTC_FORMAT:
        now = datetime.datetime.utcnow()
    else:
        now = datetime.datetime.now()

    d = datetime.timedelta(seconds=abs(seconds))
    exactDay = None
    if seconds > 0:
        exactDay = now + d
    elif seconds == 0:
        exactDay = now
    if seconds < 0:
        exactDay = now - d
    exactDay = exactDay.strftime(format)
    print("*" * 10 + " Seconds generated now " + str(seconds) + " == " + exactDay)

    return exactDay


def getDateRange(startDate="2019-01-01", endDate="2019-12-29"):
    """ this method will return a date range for report search purpose
    formatted as 2019-11-12 02:17:53 PM UTC to 2019-11-18 02:17:53 PM UTC"""
    pattern = "^\\d{4}-\\d\\d-\\d\\d$"
    if re.match(pattern, startDate) and re.match(pattern, endDate):
        startDate = startDate + " 12:00:00 PM UTC"
        endDate = endDate + " 11:59:59 AM UTC"
        searchDate = startDate + " to " + endDate
        print("*" * (10) + "search date generated =" + searchDate)
        return searchDate
    else:
        print(" given date should be in 'YYYY-MM-DD' format")
        return None


def validate_dates_are_increasing(date_format, *date_list_str):
    """ this method will take the date format and list of dates as a string
    and then validate those dates are in increasing order"""
    datetime1 = datetime.datetime.strptime(date_list_str[0], date_format)

    for count in range(1, len(date_list_str)):
        datetime2 = datetime.datetime.strptime(date_list_str[count], date_format)
        print("asserting********" + str(datetime1) + "<" + str(datetime2))
        assert datetime1 <= datetime2
        datetime1 = datetime2



def formatJson(resp):
    """ This method is take any input and format into json if possible"""
    try:
        formattedResp = json.dumps(resp.json(), indent=4)
        return formattedResp
    except:
        return resp


def uniquify(jsonBody, append=None):
    """ This method will prefix values which needs to be unique
        Will find values which are ended by {}
        and replace with either current time or given string
    """
    tempJsonBody = copy.deepcopy(jsonBody)
    if append is None:
        append = get_date_days_from_now(0, "%Y%m%d%H%M%S%f")

    keys = tempJsonBody.keys()
    for key, value in jsonBody.items():
        if isinstance(value, dict):
            subJson = uniquify(value, append)
            tempJsonBody[key] = subJson

        elif type(value) is str and value.endswith("{}"):
            tempJsonBody[key] = value.format(append)

    return tempJsonBody


def generateAllUpdateData(masterJsonData, successCode, failCode):
    """ this method will generate all sample data ==> used for update services
    """
    testTotalData = []
    # TODO write based on the input format ==> needs some time to analyze
    # (key, value, expectedCode
    testTotalData.append(("desc", "this is new text to update description prt", 200))
    return testTotalData


def getJsonFromObject(givenDicOrList, key, value=None):
    """ This method will find and return the sub-json that contains the given key=value paris
    input value must be either list or dictionary """
    value =str(value)
    obj = copy.deepcopy(givenDicOrList)
    foundObj = None
    if isinstance(obj, list):
        for subObj in obj:
            if foundObj is not None:
                break

            foundObj = getJsonFromObject(subObj, key, value)

    elif isinstance(obj, dict):
        keys = obj.keys()
        if key in keys:
            if value == str(None):
                foundObj = obj[key]


            elif str(obj[key]) == value:
                foundObj = obj

        for subKey, subValue in obj.items():
            if foundObj is not None:
                break
            if isinstance(subValue, list) or isinstance(subValue, dict):
                foundObj = getJsonFromObject(subValue, key, value)

    # return if json is found
    if foundObj:
        print("*" * 10 + " JSON FOUND for " + str(key) + ":" + value)
        print(json.dumps(foundObj, indent=4))
    else:
        print("*" * 10 + " JSON NOT FOUND for " + str(key) + ":" + value)

    return foundObj


def getAllJsonFromObject(givenDicOrList, key, value=None):
    """ This method will find and return all sub-json that contains the given key=value paris
    input value must be either list or dictionary """
    value =str(value)
    obj = copy.deepcopy(givenDicOrList)
    foundObjs = []
    if isinstance(obj, list):
        for subObj in obj:
            foundObj = getJsonFromObject(subObj, key, value)
            if foundObj:
                foundObjs.append(foundObj)

    elif isinstance(obj, dict):
        keys = obj.keys()
        if key in keys:
            if value == str(None):
                foundObj = obj[key]
            elif str(obj[key]) == value:
                foundObj = obj

        for subKey, subValue in obj.items():
            if isinstance(subValue, list) or isinstance(subValue, dict):
                foundObj = getJsonFromObject(subValue, key, value)
                if foundObj:
                    foundObjs.append(foundObj)

    # return if json is found
    if len(foundObjs) > 0:
        print("*" * 10 + str(len(foundObjs)) + " JSON FOUND for " + str(key) + ":" + value)
        for foundObj in foundObjs:
            print(json.dumps(foundObj, indent=4))
            print("-" * 10)

    else:
        print("*" * 10 + " JSON NOT FOUND for " + str(key) + ":" + value)

    return foundObjs


def getAllValuesFromJsonArray(jsonArray, key):
    """ This method will return you all values of a given key
    Search will be done only at the first sub-json
     means will not go for nested json"""
    allValues = []

    for jsonObject in jsonArray:
        try:
            allValues.append(jsonObject[key])
        except:
            continue
        # return if json is found
    if len(allValues) > 0:
        print("*" * 10 + str(len(allValues)) + " VALUES FOUND for " + str(key))
        print(allValues)
        return allValues

    else:
        print("*" * 10 + " JSON NOT FOUND for " + str(key))
        return []


def validateSchema(data, schema):
    """ Checks whether the given json matches the expected schema """
    validate(data, schema)


def loadJsonSchema(filename):
    """ This method will read Sip schema from file and load
        This will be used to validate if the response data matches  the schema"""

    relativePath = "../../schema/{}".format(filename)

    with open(relativePath) as schema_file:
        return json.loads(schema_file.read())


# TODO bellow methods not used yet
def snapshot_containers(self):
    """Snapshot all API test_function containers"""
    for container in self.lxd_client.containers.all():
        print('SNAPSHOTTING CONTAINER {}'.format(container.name))
        snapshot_id = 'test_function'
        container.snapshots.create(snapshot_id, wait=True)
        self.current_snapshots[container.name] = snapshot_id


def restore_containers(self):
    """Restore the API test_function container snapshots"""
    for container in self.current_snapshots.keys():
        print('REVERTING TEST CONTAINER {}'.format(container))
        # self.lxd_client.api1.containers[container].put(data=(dict(restore=self.current_snapshots[container])))
        subprocess.run(['lxc', 'restore', container, 'test_function'])


def delete_snapshots(self):
    """Delete the snapshots taken by pylxd"""
    for container in self.current_snapshots.keys():
        print('deleting snapshots')
        # TODO convert this to an api1 call to the daemon
        subprocess.run(['lxc', 'delete', '{0}/test_function'.format(container)], check=True)


def assertLisEquals(firstList, secondList, preserveCase=True):
    """ method to assert whether the 1st list equl to 2nd list
    :param preserveCase: will consider the case too
    """

    print("*" * 10 + "EXPECED LIST")
    print(firstList)
    print("*" * 10 + "ACTUAL LIST")
    print(secondList)
    subSet = {}
    mainSet = {}

    if not preserveCase:
        firstList = toLowerCase(firstList)
        secondList = toLowerCase(secondList)

    subSet = set(firstList)
    mainSet = set(secondList)

    assert subSet == mainSet


def assertLisContains(subList, mainList, preserveCase=True):
    """ method to assert whether the sublist is in the main list
    :param preserveCaseSensitivity: will consider the case too
    """

    print("*" * 10 + "EXPECED LIST")
    print(subList)
    print("*" * 10 + "ACTUAL LIST")
    print(mainList)
    subSet = {}
    mainSet = {}

    if not preserveCase:
        subList = toLowerCase(subList)
        mainList = toLowerCase(mainList)

    subSet = set(subList)
    mainSet = set(mainList)

    assert subSet <= mainSet


def toLowerCase(inputList):
    lowerCaseList = [(str(element)).lower() for element in inputList]
    return lowerCaseList

def generate_string(upper=0, lower=0, number=0, special=0):
    """ this method will generate random string based on the given inputs"""
    lower = [random.choice(string.ascii_lowercase) for _ in range(lower)]
    upper = [random.choice(string.ascii_uppercase) for _ in range(upper)]
    number = [random.choice(string.digits) for _ in range(number)]
    special = [random.choice(string.punctuation) for _ in range(special)]

    final_string = number + special +  lower + upper
    return "".join(final_string)

def ssh_run_cmd(command, ip, user=None, password=None):
    """ this method is used to run commands through ssh on remote linux VM
    make sure /etc/sudoers file contains the follwoing, of not write it to the end of the file
    nodeone     ALL = (ALL)     NOPASSWD: ALL
    """
    import paramiko
    tout = 20

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # create ssh connection session
    print("*" * 10 + "Connecting to " + ip + "(" + user + ", " + password + ")")
    try:
        ssh.connect(hostname=ip, username=user, password=password, timeout=tout, compress=True, look_for_keys=False,
                    allow_agent=False)
    except (socket.error, paramiko.AuthenticationException, paramiko.SSHException) as message:
        print("ERROR: SSH connection to " + ip + " failed: " + str(message))
        sys.exit(1)

    # run command
    stdin, stdout, ssh_stderr = ssh.exec_command(command)
    out = stdout.read()
    stdin.flush()
    ssh.close()
    return out


def enable_brt_licence(enable=False):
    """ thi method will enable/disable brt licence"""
    findLine = 'license_key.present? \\&\\& license_key.brta_capable?'

    command = 'sudo sed -i -e "s/.*'+findLine+'.*/'+findLine+'/" /opt/apps/verodin/planner/app/models/organization.rb'
    if enable:
        command = 'sudo sed -i -e "s/.*' + findLine + '.*/return true # changed by QA ' + findLine + '/" /opt/apps/verodin/planner/app/models/organization.rb'

    ssh_run_cmd(command)
    ssh_run_cmd('sudo vrestart') # possible to run ==> systemctl restart httpd
    sleep(60)


def print_details(method, uri, params, body):
    print("*" * 10 + method + " " + uri)
    print("Params" + "-" * 5)
    print(json.dumps(params, indent=4))
    print("Body" + "-" * 5)
    print(json.dumps(body, indent=4))


def assert_data(givenData, expectedDataList=None, notExpectedDataList=None):
    """This methods will assert if list of given strings exists in the given data or not
     before test_function all white space, double and single cotes will be removed from all imputes """

    givenString = str(givenData).replace('"', "").replace("'", "").replace(" ", "").replace("\\", "").lower()

    # validate expected data are in the given string
    if expectedDataList is not None:
        print("*" * 3 + "validate if response contains the following data")
        print(expectedDataList)

        if isinstance(expectedDataList, str):
            expectedDataList = [expectedDataList]

        for value in expectedDataList:
            value = str(value).replace('"', "").replace("'", "").replace(" ", "").replace("\\", "").lower()
            assert value in givenString

    # validate not expected data are not in the given string
    if notExpectedDataList is not None:
        print("*" * 3 + "validate if response not contains the following data")
        print(notExpectedDataList)

        if isinstance(notExpectedDataList, str):
            notExpectedDataList = [notExpectedDataList]

        for value in notExpectedDataList:
            if value == '': continue
            value = str(value).replace('"', "").replace("'", "").replace(" ", "").replace("\\", "")
            assert not value in givenString

    pass


def generate_mandatory_fields(masterJsonData):
    """ this method will prepare json with only Mandatory fields with correct data"""
    tempData = copy.deepcopy(masterJsonData)
    correctData = {}

    for attribute, passFailValues in tempData.items():
        if isinstance(passFailValues, dict):
            correctData[attribute] = generate_mandatory_fields(passFailValues)
        elif isinstance(passFailValues, tuple):
            successValues = passFailValues[0]
            if isinstance(successValues, list):
                if not successValues[0] == None:
                    correctData[attribute] = successValues[0]
            elif not successValues == None:
                correctData[attribute] = successValues

        elif isinstance(passFailValues, list):
            successValue = passFailValues[0]
            if not successValue == None:
                correctData[attribute] = successValue
        elif not passFailValues == None:
            correctData[attribute] = passFailValues

    return correctData


def generae_missing_fields(mandatoryFieldsData, failCode="422"):
    testTotalData = []
    tempData = copy.deepcopy(mandatoryFieldsData)

    # remove mandatory fields one by one
    for attribute, value in tempData.items():
        if isinstance(value, dict):
            subJsonTestData = generae_missing_fields(value, failCode)
            for subAttribute, subTestData, subFailCode in subJsonTestData:
                testData = copy.deepcopy(mandatoryFieldsData)
                testData[attribute] = subTestData
                testTotalData.append((subAttribute, testData, failCode))

        else:
            testData = copy.deepcopy(mandatoryFieldsData)
            testData.pop(attribute, "removed")
            testTotalData.append((attribute, testData, failCode))

    return testTotalData


def generate_all_fields(masterJsonData, successCode="201", failCode="422"):
    """ this method will generate all test_function Json with various data combinations
    """
    # get mandatory fields first

    mandatoryFieldsData = generate_mandatory_fields(masterJsonData)

    tempData = copy.deepcopy(masterJsonData)
    expectedCodes = (successCode, failCode)  # put in a list for iteration
    testTotalData = []  # (attribute, value, jsonBody, expectedCode)

    # remove mandatory filed data

    # populate all possible combined data
    for attribute, passFailData in tempData.items():

        if isinstance(passFailData, dict):
            # Go for subjson
            subJsonData = generate_all_fields(passFailData, successCode, failCode)
            for subAttribute, subValue, subTestData, subStatusCode in subJsonData:
                testData = copy.deepcopy(mandatoryFieldsData)
                testData[attribute] = subTestData
                testTotalData.append((subAttribute, subValue, copy.deepcopy(testData), subStatusCode))
        elif isinstance(passFailData, tuple):
            index = -1
            for values in passFailData:  # ([list of pass data], [list of fail data])
                index = index + 1  # if index =0 ==> pass data , if 1 then fail data
                expectedCode = expectedCodes[index]

                # process success or fail values
                if isinstance(values, list):

                    for value in values:
                        testData = copy.deepcopy(mandatoryFieldsData)
                        if not value == None:
                            testData[attribute] = value
                            testTotalData.append((attribute, value, testData, expectedCode))
                elif not value == None:
                    testData = copy.deepcopy(mandatoryFieldsData)
                    testData[attribute] = value
                    testTotalData.append((attribute, value, testData, expectedCode))
        elif isinstance(passFailData, list):
            for passData in passFailData:
                if not passData == None:
                    testData = copy.deepcopy(mandatoryFieldsData)
                    testData[attribute] = passData
                    testTotalData.append((attribute, passData, testData, expectedCodes[0]))
        elif not passFailData == None:
            # with data it is already tested by mandatory fields
            pass

    return testTotalData



def basicAssert(resp, expectedCode=None, expectedDataList=None):
    """Assert response code and some expected data """

    # log / print response
    print("*" * 10 + "Response")
    respData = None
    try:
        respData = resp.json()
        print(json.dumps(respData, indent=4))
    except:
        respData = resp.text
        print(respData)

    # assert status code is as expected
    actual = resp.status_code
    print("*" * 3 + "expectedCode={}".format(expectedCode) + ", actualCode={}".format(actual))
    if expectedCode is not None:
        assert int(actual) == int(expectedCode)

    # validate if each expected value are in the response
    if expectedDataList is not None:
        assert_data(respData, expectedDataList, None)

    return respData
