#!/usr/bin/env python3
"""
    Contains common api_test constants, endpoints and
    methods toGet, Put, Post, Del Methods
    Assertion methods: to validate response code, value (string / json ) and schema
"""
import copy
import json

# access
import requests

from helper.master import USER_ADMIN_SESSION, IP, PORT
from helper.utils import print_details, assert_data, load_config_file, basicAssert

BASE_URI = f"http://{IP}:{PORT}"

# response codes
STATUS_OK = 200
STATUS_CREATED = 201
STATUS_DELETED = 204
STATUS_FOUND = 302
STATUS_REQUEST_ERROR = 400
STATUS_AUTHENTICATION_FAIL = 401
STATUS_NOT_AUTHORISED = 403
STATUS_NO_URI = 404
STATUS_NOT_ALLOWED = 405
STATUS_NOT_ACCEPTABLE = 406
STATUS_INVALID_OPERATION = 409
STATUS_UNPROCESSABLE_ENTITY = 422
STATUS_TOO_MANY_REQUESTS = 429
STATUS_SERVER_ERROR = 500
STATUS_DISABLED_FEATURE = 503

# END POINTS

# SIGNUP and SIGN-IN
END_POINT_POST_SIGNUP = "/authentication/signup/"
END_POINT_POST_SIGNIN = "/authentication/signin/"

# USER END POINTS
END_POINT_GET_USERS = "/users/{}"
END_POINT_GET_CURRENT_USER = "/current_user"
END_POINT_PUT_USERS = "/users/{}"
END_POINT_DELETE_USERS = "/users/{}"



# CATEGORY END POINTS
END_POINT_POST_CATEGORIES = "/categories/"
END_POINT_GET_CATEGORIES = "/categories/{}"
END_POINT_PUT_CATEGORIES = "/categories/{}"
END_POINT_DELETE_CATEGORIES = "/categories/{}"

# CATEGORY END POINTS
END_POINT_POST_AGENDAS = "/agendas/"
END_POINT_GET_AGENDAS = "/agendas/{}"
END_POINT_PUT_AGENDAS = "/agendas/{}"
END_POINT_DELETE_AGENDAS = "/agendas/{}"

# TEMPLATE ENDPOINTS
END_POINT_POST_TEMPLATE = "/template"
END_POINT_PUT_TEMPLATE = "/template/{}"
END_POINT_GET_TEMPLATE = "/template/{}"
END_POINT_DELETE_TEMPLATE = "/template/{}"
END_POINT_POST_APPROVE_TEMPLATE = "/template/{}/approve"

# RATES END POINTS
END_POINT_POST_RATES = "/rates/{}"
END_POINT_GET_RATES = "/rates/{}"
END_POINT_PUT_RATES = "/rates/{}"


# FORUM END POINTS
END_POINT_POST_FORUMS = "/forums/{}"
END_POINT_GET_FORUMS = "/forums/{}"
END_POINT_PUT_FORUMS = "/forums/{}"

# VOTE END POINTS
END_POINT_POST_VOTES = "/votes/{}"
END_POINT_GET_VOTES = "/votes/{}"


# DASHBOARD END POINTS
END_POINT_GET_DASHBOARD = "/dashboard/"



def api_request_and_assert(method_name, end_point, params=None, body=None, expected_code=None, expected_data_list=None,
                           files=None, data=None, headers=None, return_raw_response=False, email=None, password=None,
                           bearer=None):
    """ Make an http call (put, post, delete, get) and assert expected code and expected data
        Make a basic assert
        Return body json
        methos => GET, POST, PUT, PATCH, DELETE
        end_point -- the path where the final service is exposed to
        params -- dictionary with key values that need to be passed as parameter to the given end point
        body -- dictionary of request body
        expected_code -- status code expected in api call response
        expected_data_list -- list of strings going to be check on the body response
    """
    uri = BASE_URI + end_point
    print_details("Send request to {}:".format(method_name), uri, params, body)


    methods = {'get': USER_ADMIN_SESSION.get, 'put': USER_ADMIN_SESSION.put, 'post': USER_ADMIN_SESSION.post,
               'patch': USER_ADMIN_SESSION.patch, 'delete': USER_ADMIN_SESSION.delete}

    method = methods[method_name.lower()]

    resp = method(uri, params=params, json=body, files=files, data=data, headers=headers)

    resp_data = basicAssert(resp, expected_code, expected_data_list)
    return resp_data


def post(end_point, params=None, body=None, expected_code=None, expected_data_list=None, files=None, data=None,
         headers=None, return_raw_response=False, email=None, password=None):
    return api_request_and_assert('post', end_point, params, body, expected_code, expected_data_list, files=files,
                                  data=data, headers=headers, return_raw_response=return_raw_response, email=email,
                                  password=password)


def patch(end_point, params=None, body=None, expected_code=None, expected_data_list=None, email=None, password=None):
    return api_request_and_assert('patch', end_point, params, body, expected_code, expected_data_list, email=email,
                                  password=password)


def get(end_point, params=None, body=None, expected_code=None, expected_data_list=None, headers=None,
        return_raw_response=False, email=None, password=None):
    return api_request_and_assert('get', end_point, params, body, expected_code, expected_data_list, headers=headers,
                                  return_raw_response=return_raw_response, email=email, password=password)


def put(end_point, params=None, body=None, expected_code=None, expected_data_list=None, files=None, data=None,
        headers=None, email=None, password=None):
    return api_request_and_assert('put', end_point, params, body, expected_code, expected_data_list, files=files,
                                  data=data, headers=headers, email=email, password=password)


def delete(end_point, params=None, body=None, expected_code=None, expected_data_list=None, email=None, password=None):
    return api_request_and_assert('delete', end_point, params, body, expected_code, expected_data_list, email=email, password=password)