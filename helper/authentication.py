from helper.api import END_POINT_POST_SIGNUP, post, put, END_POINT_POST_SIGNIN, STATUS_OK, \
    STATUS_CREATED, get, delete, STATUS_DELETED
SYSTEM_USER= 111111
ADMIN_USER= 222222
REGULAR_USER = 333333
EXCLUDE_IDS = [SYSTEM_USER, ADMIN_USER, REGULAR_USER]


def post_signup(body, status=None, expected_list=None):
    resp = post(END_POINT_POST_SIGNUP, None, body, status, expected_list)
    return resp


def post_signin(email, password, expected_code=None, expected_list=None):
    body = {"email": email, "password": password}
    resp = post(END_POINT_POST_SIGNIN, body=body, expected_code=expected_code)
    return resp