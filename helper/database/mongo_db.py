import pymongo as pymongo

from helper.master import MONGO_IP, MONGO_PORT, MONGO_DB

client = pymongo.MongoClient(f"mongodb://{MONGO_IP}:{MONGO_PORT}/")
db = client[MONGO_DB]

# AGENDA MONGO DB COLLECTION NAMES
COLLECTION_USERS = "users"
COLLECTION_CATEGORIES = "categories"
COLLECTION_AGENDAS = "agendas"
COLLECTION_RATES = "rates"
COLLECTION_FORUMS = "forums"
COLLECTION_VOTES = "votes"
COLLECTION_LOCKS = "locks"

# AGENDA DOCUMENT KEYS
DOCUMENT_KEY_USER_ID = "user_id"
DOCUMENT_KEY_CATEGORY_ID = "category_id"
DOCUMENT_KEY_AGENDA_ID = "agenda_id"


def update_one(table_name, filter_key=None, filter_value=None, update_key=None, update_value=None):
    """ this method will find a document from a collection using the given key and values
     and then make an update to that collection using the given json """
    if filter_key is not None and filter_value is not None and update_key is not None:
        collection = db[table_name]
        my_query = {filter_key: filter_value}
        new_values = {"$set": {update_key: update_value}}
        collection.update_one(my_query, new_values)


def delete_one(table_name, filter_key=None, filter_value=None):
    """ this method will delete a record from a table """
    collection = db[table_name]
    if filter_key is not None:
        filter = {filter_key: filter_value}
        collection.delete_one(filter)


def delete_many(table_name, filter={}):
    """
    this method will delete the filter document
    if you are not passing filter data, then it will delete all documents
    """
    collection = db[table_name]
    collection.delete_many(filter)


def delete_all_except(table_name, key, values=[]):
    """
    this method will delete all records (documents) from a collection
    except those which has the given key and value pairs
    """
    collection = db[table_name]
    query = {key: {"$nin": values}}
    resp = collection.delete_many(query)


# TODO add select documents from collections






