from helper.agenda import EXCLUDE_IDS
from helper.api import post, get, delete, STATUS_DELETED, END_POINT_POST_RATES, END_POINT_GET_RATES, END_POINT_PUT_RATES, put
from helper.database.mongo_db import delete_all_except, COLLECTION_RATES, DOCUMENT_KEY_AGENDA_ID

EXCLUDE_IDS = []

def post_rates(agenda_id, body=None, like=True, status=None, expected_list=None):
    """ user id will be taken from your login session"""
    if body == None:
        body = {"like": like}

    resp = post(END_POINT_POST_RATES.format(agenda_id), None, body, status, expected_list)
    return resp


def put_rates(agenda_id, like=True, status=None, expected_list=None):
    """ user id will be taken from your login session"""
    body = {"like": like}

    resp = put(END_POINT_PUT_RATES.format(id), None, body, status, expected_list)
    return resp

def get_rates(agenda_id="", user_id=None, expected_code=None, expected_list=[]):
    params = None
    if user_id is not None:
        params = {"user_id": user_id}
    resp = get(END_POINT_GET_RATES.format(agenda_id), params=params, expected_code=expected_code, expected_data_list=expected_list)
    return resp


def delete_all_rates(except_ids=[]):
    print("*" * 10 + "Clear all rates except the following agenda IDs")
    delete_all_except(COLLECTION_RATES, DOCUMENT_KEY_AGENDA_ID, EXCLUDE_IDS)


#TODO list please update this data based on your test scenario
POST_DATA = {
    "like": ([True, False], ["any text", 435364745, 1, 0, ""])

}
