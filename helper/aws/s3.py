import boto3 as boto3
# client or resources
import pytest
from boto3 import s3
from botocore.stub import Stubber

s3_client = boto3.resources("s3")
@pytest.fixture(autouse=True)
def s3_stub():
    with Stubber(s3.meta.client) as stubber:
        yield stubber
        stubber.assert_no_pending_resurces()
