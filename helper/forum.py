from helper.api import END_POINT_POST_FORUMS, post, END_POINT_PUT_FORUMS, put, END_POINT_GET_FORUMS, get


def post_forum(agenda_id, comment, status=None, expected_list=None):
    body = {"comment": comment}

    resp = post(END_POINT_POST_FORUMS.format(agenda_id), None, body, status, expected_list)
    return resp


def put_forum(agenda_id, comment, status=None, expected_list=None):
    body = {"comment": comment}

    resp = post(END_POINT_PUT_FORUMS.format(agenda_id), None, body, status, expected_list)
    return resp


def get_forum(agenda_id="", user_id=None, expected_code=None, expected_list=[]):
    params = None
    if user_id is not None:
        params = {"user_id": user_id}
    resp = get(END_POINT_GET_FORUMS.format(agenda_id), params=params, expected_code=expected_code, expected_data_list=expected_list)
    return resp

