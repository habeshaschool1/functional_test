from helper.api import post, get, delete, STATUS_DELETED, END_POINT_POST_AGENDAS, END_POINT_GET_AGENDAS, \
    END_POINT_DELETE_AGENDAS, END_POINT_PUT_AGENDAS, put
from helper.database.mongo_db import delete_all_except, COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, update_one

# agenda states
from helper.utils import generate_mandatory_fields, generate_string, uniquify

AGENDA_STATE_EDIT = "edit"
AGENDA_STATE_REVIEW = "review"
AGENDA_STATE_CANCEL = "cancel"
AGENDA_STATE_RATE = "rate"
AGENDA_STATE_REJECT = "reject"
AGENDA_STATE_FORUM = "forum"
AGENDA_STATE_VOTE = "vote"
AGENDA_STATE_CLOSE = "close"

# sample agendas on different states
AGENDA_ID_OF_STATE_EDIT = 1
AGENDA_ID_OF_STATE_REVIEW = 2
AGENDA_ID_OF_STATE_CANCEL = 3
AGENDA_ID_OF_STATE_RATE = 4
AGENDA_ID_OF_STATE_REJECT = 5
AGENDA_ID_OF_STATE_FORUM = 6
AGENDA_ID_OF_STATE_VOTE = 7
AGENDA_ID_OF_STATE_CLOSE = 8
EXCLUDE_IDS = [AGENDA_ID_OF_STATE_EDIT, AGENDA_ID_OF_STATE_REVIEW,
               AGENDA_ID_OF_STATE_CANCEL, AGENDA_ID_OF_STATE_RATE,
               AGENDA_ID_OF_STATE_REJECT, AGENDA_ID_OF_STATE_FORUM,
               AGENDA_ID_OF_STATE_VOTE, AGENDA_ID_OF_STATE_CLOSE]

def reset_state_of_test_agendas():
    """
    this method will make sure that agenda id from 1 to 8 is on the right state
    """
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_EDIT, update_key="state", update_value=AGENDA_STATE_EDIT)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_REVIEW, update_key="state", update_value=AGENDA_STATE_REVIEW)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_CANCEL, update_key="state", update_value=AGENDA_STATE_CANCEL)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_RATE, update_key="state", update_value=AGENDA_STATE_RATE)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_REJECT, update_key="state", update_value=AGENDA_STATE_REJECT)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_FORUM, update_key="state", update_value=AGENDA_STATE_FORUM)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_VOTE, update_key="state", update_value=AGENDA_STATE_VOTE)
    update_one("agendas", filter_key="agenda_id", filter_value=AGENDA_ID_OF_STATE_CLOSE, update_key="state", update_value=AGENDA_STATE_CLOSE)


def post_agendas(body, status=None, expected_list=None):
    resp = post(END_POINT_POST_AGENDAS, None, body, status, expected_list)
    return resp


def post_an_agenda_and_set_state(state=None, status=None):
    """ this method will create a sample agenda and also set agenda state """
    mandatoryFieldsData = generate_mandatory_fields(POST_DATA)
    post_fix_txt = generate_string(upper=4)
    payload = uniquify(mandatoryFieldsData, post_fix_txt)

    # post
    resp = post_agendas(payload, status=status)
    agenda_id = resp["result"]["agenda_id"]

    # change agenda to vote state
    if state is not None or state !=AGENDA_STATE_EDIT :
        update_one("agendas", filter_key="agenda_id", filter_value=agenda_id, update_key="state", update_value=state)

    return agenda_id


def put_agendas(id, body={}, title=None, desc=None, category_id=None, status=None, expected_list=None):
    """
    this method is to update agenda
    you may pass the full body or individual values to formulated the body for update
    """
    if title is not None:
        body["title"] = title

    if desc is not None:
        body["desc"] = desc

    if category_id is not None:
        body["category_id"] = category_id

    resp = put(END_POINT_PUT_AGENDAS.format(id), None, body, status, expected_list)
    return resp


def put_agenda_state(id, state, status=None, expected_list=[]):
    body = {"state": state}
    resp = put(END_POINT_PUT_AGENDAS.format(id), None, body, status, expected_list)
    return resp


def put_agendas_rules(id, rate_min_participants=1,
                      rate_min_like_percentage=50,
                      forum_min_participants=1,
                      status=None, expected_list=None):
    """
    this method is to update rule match for rate and forum
    """
    body = {"rules": {
            "rate": {
                "min_number_of_participants": rate_min_participants,
                "rate_like_percent": rate_min_like_percentage
             },
         "forum": {
                "min_number_of_participants": forum_min_participants
         }
         }

         }
    resp = put(END_POINT_PUT_AGENDAS.format(id), None, body, status, expected_list)
    return resp


def get_agendas(id="", expected_code=None, expected_list=[]):
    resp = get(END_POINT_GET_AGENDAS.format(id), expected_code=expected_code, expected_data_list=expected_list)
    return resp


def delete_agendas(id, expected_code=None, expectedStr=None):
    resp = delete(END_POINT_DELETE_AGENDAS.format(id), None, None, expected_code, expectedStr)
    return resp


def delete_all_agendas(except_ids=[]):
    print("*" * 10 + "Clear all agendas except the following  IDs")
    delete_all_except(COLLECTION_AGENDAS, DOCUMENT_KEY_AGENDA_ID, EXCLUDE_IDS + except_ids)


# TODO list please update this data based on your test scenario
POST_DATA = {
    "title": (["Sportf{}", "Sport1{}"], ["#$#$", 12323, "", " ", "Sport$"]),
    "desc": (["this text should be more than twenty five characters"], ["", "    ", "asdfasfdasd"]),
    "category_id" : ([1], ["any text", 435364745])

}
